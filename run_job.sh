#!/bin/bash

#curl -k --request POST --header "PRIVATE-TOKEN: $MY_TOKEN"  "https://gitlab.com/api/v4/projects/47212760/pipeline?ref=main"

curl --request POST \
        --header "PRIVATE-TOKEN: $MY_TOKEN" \
        --header "Content-Type: application/json" \
        --data "{ \"variables\": [ {\"key\": \"PORT\", \"value\": \"$PORT\"}, {\"key\": \"FULL_TAG\", \"value\": \"$FULL_TAG\"}, {\"key\": \"FULL_TAG_NGINX\", \"value\": \"$FULL_TAG_NGINX\" } ] }" \
        "https://gitlab.com/api/v4/projects/47212760/pipeline?ref=main" | jq .

L24_PIPELINE_ID=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/47212760/pipelines" | jq '.[] | .id' | head -n 1)

JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/47212760/pipelines/$L24_PIPELINE_ID/jobs" | jq '.[] | select(.name == "less24") | .id')

echo $L24_PIPELINE_ID
echo $MY_TOKEN
echo $JOB_ID
echo $FULL_TAG

curl -k --request POST --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/47212760/jobs/$JOB_ID/play"
